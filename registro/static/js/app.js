$(function(){
    $("#formulario").hide()
})

$("#Registrarse").click(function(){
    $("#formulario").show()
})
$("#ocultar").click(function(){
    $("#formulario").hide()
})


$(function(){
    $("#formulario").validate({
        rules:{
            nombre:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            run:{
                required:true,
                maxlength:10
            },
            Edad:{
                required:true,
                date:true
            },
            Telefono:{
                digits:true
            },
            region:{
                required:true
            },
            ciudad:{
                required:true
            },
            vivienda:{
                required:true
            }
        },
        messages:{
            nombre:{
                required:"Debe ingresar su nombre completo"
            },
            email:{
                required:"Debe ingresar su correo",
                email: "Debe ingresar un correo con formato correo@correo.cl"
            },
            run:{
                required:"Debe ingresar su run",
                maxlength:"Debe ingresar un run con formato 20198096-8"
            },
            Edad:{
                required:"Debe ingresar su fecha de nacimiento",
                date:"Debe seleccionar una fecha valida"
            },
            Telefono:{
                digits:"Debe ingresar solo numeros"
            },
            region:{
                required:"Debe ingresar su region"
            },
            ciudad:{
                required:"Debe ingresar su ciudad"
            },
            vivienda:{
                required: "Debe ingresar su vivienda"
            }
        }
    })
})

