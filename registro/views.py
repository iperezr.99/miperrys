from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
from .models import Animales
from django.shortcuts import redirect
from django.contrib.auth.models import User
#Sistema de autenticación
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required

# Create your views here.:


def index(request):
    usuario = request.session.get('usuario', None)
    return render(request, 'index.html', {'name': 'Registro de personas', 'usuario': usuario})


def crear(request):
    email = request.POST.get('email', '')
    run = request.POST.get('run', '')
    nombre = request.POST.get('nombre', '')
    edad = request.POST.get('edad', '00/00/0000')
    telefono = request.POST.get('telefono', 0)
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    vivienda = request.POST.get('vivienda', '')
    contrasenia = request.POST.get('contrasenia', '')
    apellido = request.POST.get('apellido', '')
    persona = Persona(nombre=nombre, email=email, run=run, edad=edad, telefono=telefono, region=region, ciudad=ciudad, vivienda=vivienda, contrasenia=contrasenia, apellido=apellido)
    persona.save()

    user = User.objects.create_user(username=nombre, password=contrasenia,
                                    email=email, first_name=nombre, last_name=apellido)
    user.save()

    return redirect('index')

def administracion(request):
    usuario = request.session.get('usuario', None)
    return render(request, 'administracion.html', {'elementos':Persona.objects.all()})

def buscar(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request, 'buscar.html', {'persona':persona})

def editar(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request, 'editar.html', {'persona':persona})

def editado(request,id):
    persona = Persona.objects.get(pk=id)
    email = request.POST.get('email', '')
    run = request.POST.get('run', '')
    nombre = request.POST.get('nombre', '')
    edad = request.POST.get('edad', '00/00/0000')
    telefono = request.POST.get('telefono', 0)
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    vivienda = request.POST.get('vivienda', '')
    persona.nombre = nombre
    persona.email = email
    persona.run = run
    persona.edad = edad
    persona.telefono = telefono
    persona.region = region
    persona.ciudad = ciudad
    persona.vivienda = vivienda
    persona.save()

    return redirect('administracion')

def eliminar(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return redirect('administracion')

#Login


def restablecer_contra(request):
    return render(request, 'restablecer_contra.html')

def restablecido(request):
    nombre_usuario = request.POST.get('nombre_usuario', '')
    apellido = request.POST.get('apellido', '')
    email = request.POST.get('email', '')
    contrasenia = request.POST.get('contrasenia', '')
    try:
        u = User.objects.get(username=nombre_usuario, email__icontains=email, last_name__icontains=apellido)
    except User.DoesNotExist:
        u = None

    if u is not None:
        print("Se han cambiado los daros del usuario"+
              str(u.pk) + " - " + u.first_name)
        u.set_password(contrasenia)
        u.save()
        return redirect('login')
    else:
        print(" Error en datos !!")
        return redirect('restablecer_contra')


def login(request):
    return render(request, 'login.html', {})

def login_iniciar(request):
    nombre_usuario = request.POST.get('nombre_usuario', '')
    contrasenia = request.POST.get('contrasenia', '')
    user = authenticate(username=nombre_usuario, password=contrasenia)
    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect('index')
    else:
        return redirect('login')

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')



#Crud de animales


@login_required(login_url='login')
def crear_animal(request):
    return render(request, 'crear_animal.html', {})

@login_required(login_url='login')
def ingresar(request):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    descripcion = request.POST.get('descrip', '')
    foto = request.FILES['foto']
    print(foto.content_type)
    animales = Animales(nombre=nombre, foto=foto, raza=raza, descripcion=descripcion, estado=1)
    animales.save()
    return redirect('crear_animal')

def galeria(request):
    return render(request, 'galeria.html', {'animales': Animales.objects.all()})

@login_required(login_url='login')
@staff_member_required(login_url='login')
def adm_animales(request):
    return render(request, 'adm_animales.html', {'animales': Animales.objects.all()})

@login_required(login_url='login')
@staff_member_required(login_url='login')
def editar_animal(request,id):
    animales = Animales.objects.get(pk=id)
    return render(request, 'editar_animal.html', {'animales': animales})

@login_required(login_url='login')
@staff_member_required(login_url='login')
def animal_editado(request,id):
    animales = Animales.objects.get(pk=id)
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    descripcion = request.POST.get('descrip', '')
    foto = request.FILES['foto']
    estado = request.POST.get('estado', '')
    print(foto.content_type)
    animales.nombre = nombre
    animales.raza = raza
    animales.descripcion = descripcion
    animales.foto = foto
    animales.estado = estado
    animales.save()
    return redirect('adm_animales')

@login_required(login_url='login')
@staff_member_required(login_url='login')
def eliminar_animal(request,id):
    animales = Animales.objects.get(pk=id)
    animales.delete()
    return redirect("adm_animales")









