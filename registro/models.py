from django.db import models

# Create your models here.
class Persona(models.Model):
    nombre = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    run = models.CharField(max_length=10)
    edad = models.DateField()
    telefono = models.IntegerField()
    region = models.IntegerField()
    ciudad = models.IntegerField()
    vivienda = models.IntegerField()
    contrasenia= models.CharField(max_length=30)
    apellido = models.CharField(max_length=100)


class Animales(models.Model):
    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)
    foto = models.ImageField(upload_to='photos/')





