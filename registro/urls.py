from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.index, name="index"),
    path('login/', views.login, name="login"),
    path('cerrar_session/', views.cerrar_session, name="cerrar_session"),
    path('login/login_iniciar', views.login_iniciar, name="login_iniciar"),
    path('crear/', views.crear, name="crear"),
    path('administracion/', views.administracion, name="administracion"),
    path('administracion/buscar/<int:id>', views.buscar, name="buscar"),
    path('administracion/editar/<int:id>', views.editar, name="editar"),
    path('administracion/editado/<int:id>', views.editado, name="editado"),
    path('administracion/eliminar/<int:id>', views.eliminar, name="eliminar"),
    path('crear_animal', views.crear_animal, name="crear_animal"),
    path('crear_animal/ingresar', views.ingresar, name="ingresar"),
    path('galeria/', views.galeria, name='galeria'),
    path('adm_animales/', views.adm_animales, name="adm_animales"),
    path('adm_animales/editar_animal/<int:id>', views.editar_animal, name="editar_animal"),
    path('adm_animales/animal_editado/<int:id>', views.animal_editado, name="animal_editado"),
    path('adm_animales/eliminar_animal/<int:id>', views.eliminar_animal, name="eliminar_animal"),
    path('restablecer_contra/', views.restablecer_contra, name="restablecer_contra"),
    path('restablecido/', views.restablecido, name="restablecido"),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    path('accounts/', include('django.contrib.auth.urls')),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)